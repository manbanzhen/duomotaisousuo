import './assets/main.scss'
import "animate.css";
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(router)
// app.use(animated)

app.mount('#app')
