import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '',
      component: () => import('../layout/default.vue'),
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'home',
          component: () => import('../views/home.vue'),
          meta: {
            title: '首页'
          }
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/login.vue'),
      meta: {
        title: '登录'
      }
    },
      // 产品服务
    {
      path: '/products_services',
      component: () => import('../layout/default.vue'),
      redirect: '/products_services/media_img',
      children: [
        {
          path: 'media_img',
          name: 'products_services/media_img',
          component: () => import('../views/productsServices/media_img.vue'),
          meta: {
            title: '智能审核'
          }
        },
        {
          path: 'media_file',
          name: 'products_services/media_file',
          component: () => import('../views/productsServices/media_file.vue'),
          meta: {
            title: '内容识别'
          }
        },
        {
          path: 'ocr',
          name: 'products_services/ocr',
          component: () => import('../views/productsServices/ocr.vue'),
          meta: {
            title: '手写体文字识别'
          }
        },
        {
          path: 'multimodalSearch',
          name: 'products_services/multimodalSearch',
          component: () => import('../views/productsServices/multimodalSearch.vue'),
          meta: {
            title: '多模态检索'
          }
        }
      ]

    },
      // 解决方案
    {
      path: '/solution',
      component: () => import('../layout/default.vue'),
      redirect: '/solution/media',
      children: [
        {
          path: 'media',
          name: 'solution/media',
          component: () => import('../views/solution/media.vue'),
          meta: {
            title: '内容审核解决方案'
          }
        },
        {
          path: 'marketing',
          name: 'solution/marketing',
          component: () => import('../views/solution/marketing.vue'),
          meta: {
            title: '精准营销解决方案'
          }
        },
        {
          path: 'search',
          name: 'solution/search',
          component: () => import('../views/solution/search.vue'),
          meta: {
            title: '智能搜索解决方案'
          }
        }
      ]
    },
      // 试用
    {
      path: '/tryout',
      component: () => import('../layout/default.vue'),
      redirect: '/tryout/upload',
      children: [
        {
          path: 'upload',
          name: 'tryout/upload',
          component: () => import('../views/tryout/upload.vue'),
          meta: {
            title: '产品试用'
          }
        },
        {
          path: 'multimodal_search',
          name: 'tryout/multimodal_search',
          component: () => import('../views/tryout/multimodal_search.vue'),
          meta: {
            title: '多模态搜索 - 产品试用'
          }
        },
        {
          path: 'precisionMarketing',
          name: 'tryout/precisionMarketing',
          component: () => import('../views/tryout/precisionMarketing.vue'),
          meta: {
            title: '精准营销 - 产品试用'
          }
        },

        {
          path: 'multimodal_audits',
          name: 'tryout/multimodal_audits',
          component: () => import('../views/tryout/multimodal_audits.vue'),
          meta: {
            title: '多模态审核 - 产品试用'
          }
        },
        {
          path: 'content_identification',
          name: 'tryout/content_identification',
          component: () => import('../views/tryout/content_identification.vue'),
          meta: {
            title: '内容识别 - 产品试用'
          }
        },
        {
          path: 'ocr',
          name: 'tryout/ocr',
          component: () => import('../views/tryout/ocr.vue'),
          meta: {
            title: '手写体文字识别 - 产品试用'
          }
        },
      ]
    }
  ]
})

export default router


router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})
