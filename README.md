# AIM-α 项目

## 技术栈
`Vue3`, `Vite`, `SCSS`


> root-password: duomotai
## 本地运行、打包：
1.	克隆项目  
2.	下载依赖  

```
npm i
```
3.	运行  
```
npm run dev
```
4.	打包  
```
npm run build
```

## 部署：
1.	安装Nginx:
```
sudo apt-get install nginx
```
2. 配置：
```
server {
    listen 8800;
    server_name     43.138.100.98;
    location / {
        root    /home/duomotai;
        index   index.html;
        try_files       $uri $uri/ /index.html;
    } 
}
```
2.	上传文件
将打包后的`dist`文件夹内容，上传到服务器`/home/duomotai/`文件夹下即可。



## 文件目录：

-	dist -------------------------打包后的文件目录
-	node_modules ----------------依赖包管理
-	public ----------------------- 部署后根目录文件
    *	demo_images
        - productsServices/ ----- 产品页面中功能演示图片
	    * favicon.ico ---------------- 浏览器小图标
-	src -------------------------- 项目代码目录  
    * assets -------------------  静态文件
       - images ---------------  全局图片
        > （与views页面文件夹相对应）

        common -----------  layout全局模板图  
    	home ------------- 首页图片  
    	login -------------- 登录页图片  
    	products_services ---- 产品图片  
    	solution ------------ 解决方案图片  
    	tryout -------------- 产品试用页图片  
    	main.scss ------------- 配置全局默认样式   
    	normalize.scss -------- 规范化代码配置  
    	reset.scss ------------ 配置rem根元素尺寸和版心大小  
    * omponents -------------- 全局可用组件
    	- productDemo ---------- 产品演示组件
    * layout -------------------- 页面模板
    	- components
    	    footerBlock.vue ------- 页面公共脚部组件
    	    headerBlock.vue ------ 页面公共头部组件
    	- default.vue	------------- 默认模板组件
	* router ---------------------- 页面路由配置
    * utils	----------------------- 工具类方法
    * views ----------------------- 页面代码
    	- home ------------------ 首页各个组件  
    	        components:  
                    applicationScenarios.vue -----首页应用场景  
                    banner.vue----------------首页banner模块  
                    coreTechnology.vue -------首页核心技术模块  
                    productSystem.vue --------首页产品体系模块  
    	- productServices ---------  产品  
                media_file.vue ------------产品内容识别页  
                media_img.vue ----------- 产品智能审核页  
                multimodalSearch.vue -----产品多模态搜索页  
                ocr.vue ----------------产品手写文字识别页  
    	- solution ------------------ 解决方案  
                marketing.vue --------- 精准营销解决方案  
                media.vue ------------ 内容审核解决方案  
                search.vue ------------ 智能搜索解决方案  
    	- tryout -------------------- 产品试用  
            multimodal_search.vue ---- 多模态搜索试用  
            upload.vue ----------- 多模态审核和OCR识别 试用页面  
    	- home.vue	--------------- 首页入口
    	- login.vue	--------------- 登录页面
    	- App.vue --------------------- 程序页面入口
    	- main.js -----------------------程序入口文件
-	.gitgnore	--------------------- git忽略配置文件
-	index.html ------------------- 项目入口html页面
-	package.json ------------------ 依赖包配置
-	vite.config.js ------------------- 项目打包vite的相关配置